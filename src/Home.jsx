import { Button, Input } from "@smaransingh/ui-library";
import React, { useState } from "react";

const Home = () => {
  const [text, setText] = useState("");

  return (
    <>
      <h2>Home</h2>
      <div>
        <>
          <Input onChange={e => setText(e.target.value)} />
          <Button onClick={() => alert(text || "Enter some value to see it here")}>
            Click me!
          </Button>
        </>
      </div>
    </>
  );
};

export default Home;
